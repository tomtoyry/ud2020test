extends KinematicBody2D

var velocity = Vector2()
const acceleration = Vector2(0,-10)
const braking = 20
const resistances = 2 # ground and air friction, constant for simplicity.
# deaceleration force of wheels when not in the same 
const wheelDriftResistance = 5 #direction as to velocity.
const maxSpeed = 600
const rotationBaseSpeed = .04
var rotationSpeed
# minimal percentage of angle difference. If lower - velocity direction
const minWDrResStr = 0.04 # changed to car direction. 1 = PI/2, should be less then rotationSpeed
const spriteAngle = -PI/2 # default sprite image and thus acceleration vector angle
const spriteSize = Vector2(32,64)

var throttle = false
var shaker = 0
var rpm = 500

func _ready():
	pass

func _process(delta):
	pass

func _physics_process(_delta):
	# RPM meter pointer control # default value -2.25 makes rpm to point 0
	$Camera2D/Interface/GUi/Viisari/e_Viisari.set_rotation(-2.25 + (float(rpm)/1000)/1.8)
	
	if shaker == 0:
		rpm = rpm - 200
		shaker += 1
	elif shaker == 3:
		rpm = rpm + 200
		shaker += 1
	elif shaker == 6:
		shaker = 0
	elif shaker > 0:
		shaker += 1
	
	
	if(throttle):
		if(rpm < 7800):
				rpm = rpm + 100;
	elif(rpm > 500):
		rpm = rpm - 150
	
	rotationSpeed = rotationBaseSpeed * velocity.length() * 0.005
	if rotationSpeed > 0.06:
		rotationSpeed = 0.06
	
	var res = resistances
	if Input.is_action_pressed('ui_left') or Input.is_action_pressed('ui_right'):
		if Input.is_action_pressed('ui_left'):
			$CollisionShape2D/Sprite.rotation -= rotationSpeed
		else:
			$CollisionShape2D/Sprite.rotation += rotationSpeed
	if Input.is_action_pressed('ui_up'):
		velocity += acceleration.rotated($CollisionShape2D/Sprite.rotation)
		throttle = true
	else:
		throttle = false
		if Input.is_action_pressed('ui_down'):
			# pressing down is equivalent to braking, no reverse gear for simplicity
			res += braking
	if velocity.length() > res:
		# angle between where car is facing and where it actually moves (drifting)
		var angle = velocity.angle()-($CollisionShape2D/Sprite.rotation-spriteAngle)
		# strength of wheel resistance to drift, strongest if drift perpendicular to car facing
		var wDrResStr = sin(angle)
		if abs(wDrResStr) > minWDrResStr:
			velocity += Vector2(wDrResStr*wheelDriftResistance,0).rotated($CollisionShape2D/Sprite.rotation)
		else: # if angle difference in minimal, then we discard
		# drift portion of velocity and leave the other making car move where it faces
			velocity *= abs(cos(angle))
		# applying triction and braking, if we brake
		velocity += -velocity.normalized() * res
		if velocity.length() > maxSpeed:
			# limiting our velocity to maxSpeed
			velocity = velocity / velocity.length() * maxSpeed
	else:
		velocity = Vector2()
	# let's not forget to save remainder of velocity, so it can be processed in future frames,
	# if we don't do it, then we will not be able to slide/drift and accelerate more
	velocity = move_and_slide(velocity)
